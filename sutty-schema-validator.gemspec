# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = 'sutty-schema-validator'
  spec.version       = '0.1.0'
  spec.authors       = ['Sutty']
  spec.email         = ['sutty@riseup.net']
  spec.summary       = 'Schema validator for Sutty and Jekyll'
  spec.homepage      = 'https://0xacab.org/sutty/sutty-schema-validator'
  spec.license       = 'Custom'
  spec.require_paths = %w[lib]
  spec.files         = Dir['lib/**/*',
                           'LICENSE*',
                           'README*',
                           'CHANGELOG*']

  spec.extra_rdoc_files = Dir['README.md', 'CHANGELOG.md', 'LICENSE.txt']
  spec.rdoc_options += [
    '--title', "#{spec.name} - #{spec.summary}",
    '--main', 'README.md',
    '--line-numbers',
    '--inline-source',
    '--quiet'
  ]
  spec.metadata = {
    'bug_tracker_uri' => "#{spec.homepage}/issues",
    'homepage_uri' => spec.homepage,
    'source_code_uri' => spec.homepage,
    'changelog_uri' => "#{spec.homepage}/-/blob/master/CHANGELOG.md",
    'documentation_uri' => "https://rubydoc.info/gems/#{spec.name}"
  }

  spec.required_ruby_version = Gem::Requirement.new('>= 2.7')

  spec.add_runtime_dependency 'bcrypt'
  spec.add_runtime_dependency 'dry-schema'
  spec.add_runtime_dependency 'email_address', '~> 0.2.0'
  spec.add_runtime_dependency 'jekyll'
  spec.add_runtime_dependency 'marcel'
  spec.add_runtime_dependency 'net-smtp'
  spec.add_runtime_dependency 'rails-html-sanitizer'

  spec.add_development_dependency 'addressable', '~> 2.7'
  spec.add_development_dependency 'pry', '~> 0.14.0'
  spec.add_development_dependency 'rspec', '~> 3.6', '>= 3.6.0'
  spec.add_development_dependency 'rspec-tap-formatters'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'yard'
end
