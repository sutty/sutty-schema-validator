# frozen_string_literal: true

require_relative 'validator'
require_relative 'types'

module Sutty
  module Schema
    # Validates Jekyll documents
    class DocumentValidator
      include Validator

      # @return [Jekyll::Document]
      attr_reader :document

      # @return [Hash]
      attr_reader :yaml_schema

      # @param :document [Jekyll::Document]
      # @param :yaml_schema [Hash]
      def initialize(document, yaml_schema)
        @document = document
        @yaml_schema = yaml_schema
      end

      # Generates a DRY Schema from a YAML Schema, to validate
      # themselves.  A `type` attribute is required.
      #
      # @todo Raise an error when type is missing
      # @return [Dry::Schema::Params]
      def dry_schema
        @dry_schema ||=
          begin
            yaml_schema.each_pair do |attribute_name, attribute_schema|
              type = attribute_schema['type']
              attribute = attribute_for(type)
              attribute_name = attribute_name.to_sym

              attribute&.to_document_schema(dry_schema_dsl, attribute_name, attribute_schema)
            end

            dry_schema_dsl.call
          end
      end

      # Process the schema and validates it
      #
      # @todo Validate content too
      # @return [Dry::Schema::Result]
      def validate
        @validate ||= dry_schema.call(document.data)
      end
    end
  end
end
