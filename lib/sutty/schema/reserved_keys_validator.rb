# frozen_string_literal: true

require_relative 'validator'

module Sutty
  module Schema
    class ReservedKeysValidator
      include Validator

      # @return [Symbol]
      attr_reader :name

      # @return [Hash]
      attr_reader :yaml_schema

      # @see Jekyll::Document#to_liquid
      # @return [Array]
      RESERVED_KEYS = %w[
        collection
        ext
        id
        next
        output
        path
        previous
        relative_path
        slug
        url
      ].freeze

      # @param :name [String,Symbol]
      # @param :yaml_schema [Hash]
      def initialize(name, yaml_schema)
        @name = name.to_sym
        @yaml_schema = yaml_schema.to_h
      end

      # Generates a DRY Schema from a YAML Schema, to validate
      # keys.
      #
      # @return [Dry::Schema::Params]
      def dry_schema
        @dry_schema ||=
          begin
            dry_schema_dsl.required(:keys).array(excluded_from?: RESERVED_KEYS)

            dry_schema_dsl.call
          end
      end

      # Process the schema and validates it
      #
      # @return [Dry::Schema::Result]
      def validate
        @validate ||= dry_schema.call({ keys: yaml_schema.keys })
      end
    end
  end
end
