# frozen_string_literal: true

require_relative 'validator'

module Sutty
  module Schema
    # Validates schemas
    class SchemaValidator
      include Validator

      # @todo Automatically register types
      AVAILABLE_TYPES = %w[
        array
        belongs_to
        boolean
        color
        content
        date
        email
        color
        content
        file
        float
        has_and_belongs_to_many
        has_many
        hidden
        html
        image
        locales
        markdown
        markdown_content
        new_content
        non_geo
        number
        order
        password
        permalink
        permalink_array
        predefined_array
        predefined_value
        price
        related_posts
        sku
        slug
        string
        tel
        text
        time
        uuid
        url
      ].freeze

      # @return [Symbol]
      attr_reader :name

      # @return [Hash]
      attr_reader :yaml_schema

      # @return [Array<Symbol>]
      attr_reader :locales

      # @param :name [String,Symbol]
      # @param :yaml_schema [Hash]
      # @param :locales [Array<Symbol,String>]
      def initialize(name, yaml_schema, locales)
        @name = name.to_sym
        @yaml_schema = yaml_schema.to_h
        @locales = locales.map(&:to_sym)
      end

      # Generates a DRY Schema from a YAML Schema, to validate
      # themselves.  A `type` attribute is required.
      #
      # @return [Dry::Schema::Params]
      def dry_schema
        @dry_schema ||=
          begin
            yaml_schema.each_pair do |attribute_name, attribute_schema|
              locales = @locales
              type = attribute_schema['type']
              attribute = attribute_for(type)
              attribute_name = attribute_name.to_sym

              # XXX: This block changes the context to the DSL
              dry_schema_dsl.required(attribute_name).hash do |dry_schema|
                dry_schema.required(:type).value(included_in?: AVAILABLE_TYPES)
                dry_schema.optional(:required).filled(:bool)

                attribute&.to_schema(dry_schema, attribute_name, attribute_schema, locales)
              end
            end

            dry_schema_dsl.call
          end
      end

      # Process the schema and validates it
      #
      # @return [Dry::Schema::Result]
      def validate
        @validate ||= dry_schema.call(yaml_schema)
      end
    end
  end
end
