# frozen_string_literal: true

module Sutty
  module Schema
    module Types
      # Validate colors
      Color = Dry::Types['string'].constrained(format: /\A#[a-f0-9]{6}\z/)
    end
  end
end
