# frozen_string_literal: true

require_relative 'permalink'

module Sutty
  module Schema
    module Types
      # Array of unique, non-empty Permalinks
      #
      # rubocop:disable Style/SymbolProc
      PermalinkArray = Dry::Types['unique_array'].constructor do |array|
        array
        .map do |string|
          Sutty::Schema::Types::Permalink[string]
        end
      end
      # rubocop:enable Style/SymbolProc
    end
  end
end
