# frozen_string_literal: true

require_relative 'plain_text_array'

module Sutty
  module Schema
    module Types
      # Array of unique, non-empty values
      #
      # rubocop:disable Style/SymbolProc
      UniqueArray = Dry::Types['plain_text_array'].constructor do |array|
        array.uniq
      end
      # rubocop:enable Style/SymbolProc
    end
  end
end
