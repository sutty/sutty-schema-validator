# frozen_string_literal: true

require 'addressable/uri'

module Sutty
  module Schema
    module Types
      # Parse & normalize url
      NormalizedUri = Dry::Types['normalized_string'].constructor do |url|
        Addressable::URI.parse(url).normalize.to_s
      end
    end
  end
end
