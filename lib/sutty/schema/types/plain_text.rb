# frozen_string_literal: true

require 'loofah'
require 'rails/html/sanitizer'
require 'rails/html/scrubbers'

module Sutty
  module Schema
    module Types
      # Sanitize strings by removing any HTML tags they may have
      PlainText = Dry::Types['normalized_string'].constructor do |string|
        Rails::HTML5::Sanitizer.full_sanitizer.new.sanitize(string).strip
      end
    end
  end
end
