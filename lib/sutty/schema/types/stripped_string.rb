# frozen_string_literal: true

module Sutty
  module Schema
    module Types
      # Remove spaces before and after, and carriage return from strings
      StrippedString = Dry::Types['coercible.string'].constructor do |string|
        string.tr("\r", '').strip
      end
    end
  end
end
