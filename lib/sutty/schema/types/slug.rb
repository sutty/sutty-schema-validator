# frozen_string_literal: true

require 'jekyll/utils'

module Sutty
  module Schema
    module Types
      # Each mode is it's own type, so we can call them without
      # parameters.
      Jekyll::Utils::SLUGIFY_MODES.each do |mode|
        class_name = "Slug#{mode.capitalize}"
        klass = self.const_set(class_name, Dry::Types['plain_text'].constructor do |string|
          Jekyll::Utils.slugify(string, mode: mode)
        end)

        Dry::Types.register("slug_#{mode}", klass)
      end
    end
  end
end
