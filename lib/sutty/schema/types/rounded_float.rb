# frozen_string_literal: true

module Sutty
    module Schema
      module Types
        # Rounds float numbers to two decimals
        RoundedFloat = Dry::Types['coercible.float'].constructor do |float|
          float.round(2)
        end
      end
    end
  end
  