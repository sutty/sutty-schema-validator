# frozen_string_literal: true

module Sutty
  module Schema
    module Types
      # Unicode strings can use different ways to write accents, so we
      # normalize them.
      NormalizedString = Dry::Types['stripped_string'].constructor(&:unicode_normalize)
    end
  end
end
