# frozen_string_literal: true

module Sutty
  module Schema
    module Types
      # Validate permalinks
      Permalink = Dry::Types['normalized_string'].constructor do |string|
        str = string.chars.map(&:strip).join
        Pathname.new(str).expand_path.to_s
      end
    end
  end
end
