# frozen_string_literal: true

require_relative 'plain_text'

module Sutty
  module Schema
    module Types
      # Sanitize array of strings by removing any HTML tags and empty values they may have
      PlainTextArray = Dry::Types['array'].constructor do |array|
        array
          .reject(&:nil?)
          .map do |string|
            Sutty::Schema::Types::PlainText[string]
          end
          .reject(&:empty?)
      end
    end
  end
end
