# frozen_string_literal: true

require 'loofah'
require 'rails/html/sanitizer'
require 'rails/html/scrubbers'

module Sutty
  module Schema
    module Types
      ALLOWED_ATTRIBUTES = %w[id name style href src alt controls
                              data-align data-multimedia data-multimedia-inner].freeze
      ALLOWED_TAGS = %w[
        p h1 h2 h3 h4 h5 h6 ul ol li div blockquote
        img iframe audio video
        figure figcaption
        strong em del u mark a sub sup small
      ].freeze

      # Sanitize strings
      SanitizedString = Dry::Types['normalized_string'].constructor do |string|
        Rails::HTML5::Sanitizer.safe_list_sanitizer.new.sanitize(
          string,
          tags: Sutty::Schema::Types::ALLOWED_TAGS,
          attributes: Sutty::Schema::Types::ALLOWED_ATTRIBUTES
        ).strip
      end
    end
  end
end
