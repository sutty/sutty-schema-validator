# frozen_string_literal: true

require 'bcrypt'

module Sutty
  module Schema
    module Types
      # Hash string that is not already hashed
      Bcrypt = Dry::Types['string'].constructor do |string|
        if string.start_with? '$2a$'
          string
        else
          ::BCrypt::Password.create(string).to_s
        end
      end
    end
  end
end
