# frozen_string_literal: true

require_relative 'attributes/label'
require_relative 'attributes/help'

module Sutty
  module Schema
    # Common methods for attribute schemas
    module Attributes
      # Complete the schema with label and help.
      #
      # @param :dry_schema_dsl [Dry::Schema::DSL]
      # @param :name [Symbol]
      # @param :yaml_schema [Hash]
      # @param :locales [Array]
      def to_schema(dry_schema_dsl, _name, _yaml_schema, locales)
        Label.to_schema(dry_schema_dsl, nil, nil, locales)
        Help.to_schema(dry_schema_dsl, nil, nil, locales)
      end

      private

      # @param :yaml_schema [Hash]
      # @return [Symbol]
      def optional_or_required(yaml_schema)
        yaml_schema['required'] ? :required : :optional
      end
    end
  end
end
