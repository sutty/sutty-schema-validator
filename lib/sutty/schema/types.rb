# frozen_string_literal: true

require 'dry/types'

module Sutty
  module Schema
    # Type namespace
    module Types
    end
  end
end

require_relative 'types/color'
Dry::Types.register('color', Sutty::Schema::Types::Color)

require_relative 'types/stripped_string'
Dry::Types.register('stripped_string', Sutty::Schema::Types::StrippedString)

require_relative 'types/normalized_string'
Dry::Types.register('normalized_string', Sutty::Schema::Types::NormalizedString)

require_relative 'types/sanitized_string'
Dry::Types.register('sanitized_string', Sutty::Schema::Types::SanitizedString)

require_relative 'types/plain_text'
Dry::Types.register('plain_text', Sutty::Schema::Types::PlainText)

require_relative 'types/plain_text_array'
Dry::Types.register('plain_text_array', Sutty::Schema::Types::PlainTextArray)

require_relative 'types/normalized_uri'
Dry::Types.register('normalized_uri', Sutty::Schema::Types::NormalizedUri)

require_relative 'types/unique_array'
Dry::Types.register('unique_array', Sutty::Schema::Types::UniqueArray)

require_relative 'types/bcrypt'
Dry::Types.register('bcrypt', Sutty::Schema::Types::Bcrypt)

require_relative 'types/permalink'
Dry::Types.register('permalink', Sutty::Schema::Types::Permalink)

require_relative 'types/permalink_array'
Dry::Types.register('permalink_array', Sutty::Schema::Types::PermalinkArray)

require_relative 'types/rounded_float'
Dry::Types.register('rounded_float', Sutty::Schema::Types::RoundedFloat)

# @todo make other types self-register too
require_relative 'types/slug'
