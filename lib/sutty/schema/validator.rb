# frozen_string_literal: true

require 'dry/schema'
require_relative 'predicates'
require_relative 'attributes/array'
require_relative 'attributes/belongs_to'
require_relative 'attributes/boolean'
require_relative 'attributes/color'
require_relative 'attributes/content'
require_relative 'attributes/date'
require_relative 'attributes/email'
require_relative 'attributes/has_and_belongs_to_many'
require_relative 'attributes/has_many'
require_relative 'attributes/file'
require_relative 'attributes/float'
require_relative 'attributes/hidden'
require_relative 'attributes/html'
require_relative 'attributes/image'
require_relative 'attributes/locales'
require_relative 'attributes/markdown'
require_relative 'attributes/markdown_content'
require_relative 'attributes/new_content'
require_relative 'attributes/non_geo'
require_relative 'attributes/number'
require_relative 'attributes/related_posts'
require_relative 'attributes/order'
require_relative 'attributes/password'
require_relative 'attributes/permalink'
require_relative 'attributes/permalink_array'
require_relative 'attributes/predefined_array'
require_relative 'attributes/predefined_value'
require_relative 'attributes/price'
require_relative 'attributes/sku'
require_relative 'attributes/slug'
require_relative 'attributes/string'
require_relative 'attributes/tel'
require_relative 'attributes/text'
require_relative 'attributes/time'
require_relative 'attributes/url'
require_relative 'attributes/uuid'

module Sutty
  module Schema
    # Common methods for validators
    module Validator
      # @return [Dry::Schema::Params]
      def dry_schema
        raise NotImplementedError
      end

      # Is the schema valid?
      #
      # @return [Boolean]
      def valid?
        validate.success?
      end

      # Return errors after validation
      #
      # @return [Hash]
      def errors
        validate.errors(full: true).to_h
      end

      # Process the schema and validates it
      #
      # @return [Dry::Schema::Result]
      def validate
        raise NotImplementedError
      end

      private

      # @param :string [String]
      # @return [String]
      def camelize(string)
        string.split('_').map(&:capitalize).join
      end

      # Obtains the attribute validator for a given type.  It can return
      # nil because we're detecting invalid types afterwards.
      #
      # @return [Object,nil]
      def attribute_for(type)
        Object.const_get("Sutty::Schema::Attributes::#{camelize type}")
      rescue NameError
      end

      # @return [Dry::Schema::DSL]
      def dry_schema_dsl
        @dry_schema_dsl ||= Dry::Schema::DSL.new(processor_type: Dry::Schema::Params).tap do |dsl|
          dsl.config.validate_keys = true
          dsl.config.messages.load_paths << File.join(File.dirname(__FILE__), '..', '..', '..', 'config', 'errors.yml')
        end
      end
    end
  end
end
