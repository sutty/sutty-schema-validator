# frozen_string_literal: true

require 'dry/logic/predicates'
require 'email_address'
require 'marcel'
require 'pathname'

module Sutty
  module Schema
    # Defines custom predicates.
    #
    # Official documentation is confusing on how they should be added,
    # and "domain-specific" predicates should be rules on
    # Dry::Validation
    #
    # @see {https://dry-rb.org/gems/dry-logic/1.5/custom-predicates/}
    module Predicates
      GIT_LFS_POINTER = %r{\Aversion https://git-lfs.github.com/spec/v1\n\z}.freeze

      WEB_IMAGES = %w[
        image/gif
        image/jpeg
        image/jpg
        image/png
        image/webp
      ].freeze

      PROTOCOLS = %w[
        https 
        ipns
        hyper
        bitcoin
        ftp
        ftps
        geo
        im
        irc
        ircs
        magnet
        mailto
        matrix
        mms
        news
        nntp
        openpgp4fpr 
        sftp
        sip
        sms
        smsto
        ssh
        tel
        urn
        webcal
        wtai
        xmpp
      ].freeze

      EXTENSIONS = ["", ".htm", ".html"]

      # File exists
      #
      # @param :input [String]
      # @return [Boolean]
      def exist?(input)
        File.exist?(input)
      end

      # File is a Git LFS pointer
      #
      # @param :input [String]
      # @return [Boolean]
      def git_lfs_pointer?(input)
        !!(GIT_LFS_POINTER =~ File.open(input, 'rb', &:gets))
      end

      # File is not a Git LFS pointer
      #
      # @param :input [String]
      # @return [Boolean]
      def not_git_lfs_pointer?(input)
        !git_lfs_pointer?(input)
      end

      # Detects if it's a web image
      #
      # @param :input [String]
      # @return [Boolean]
      def web_image?(input)
        WEB_IMAGES.include?(Marcel::MimeType.for(Pathname.new(input)))
      end

      # Validates e-mail address
      #
      # @todo Error messages are lost
      # @param :input [String]
      # @return [Boolean]
      def email?(input)
        EmailAddress::Address.new(input, {}).valid?
      end

      # Validates url and presence of host
      #
      # @param :input [String]
      # @return [Boolean]
      def uri_with_host?(input)
        uri?(PROTOCOLS, input) && !URI.parse(input).host.empty?
      end

      # Detects a valid path
      #
      # @param :input [String]
      # @return [Boolean]
      def path?(input)
        return false unless input.start_with?('/')
        return false unless max_bytesize?(2000, input)

        # check content between "/.../"
        array = input.split('/')
        last, = array.pop.split('?', 2)
        array << last

        valid = array.none? do |part|
          !max_bytesize?(255, part) || part.start_with?('-') || includes?('?', part) || includes?('#', part)
        end

        valid && includes?(File.extname(input), EXTENSIONS)
      end
    end
  end
end

Dry::Logic::Predicates.extend(Sutty::Schema::Predicates)
