# frozen_string_literal: true

require_relative 'locale'

module Sutty
  module Schema
    module Attributes
      # Labels are human-readable description for attributes.
      # Validation requires a list of locales supported.
      module Label
        extend Sutty::Schema::Attributes

        module_function

        def to_schema(dry_schema_dsl, _name, _, locales)
          dry_schema_dsl.required(:label).hash do |dry_schema|
            locales.each do |locale|
              Locale.to_schema(dry_schema, locale, nil, nil)
            end
          end
        end
      end
    end
  end
end
