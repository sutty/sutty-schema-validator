# frozen_string_literal: true

require_relative '../attributes'
require_relative 'locale'

module Sutty
  module Schema
    module Attributes
      # Validate Array schemas
      module PredefinedArray
        extend Sutty::Schema::Attributes

        module_function

        # Complete the schema with label and help, and locale for each
        # possible value.
        #
        # @param :dry_schema_dsl [Dry::Schema::DSL]
        # @param :name [Symbol]
        # @param :yaml_schema [Hash]
        def to_schema(dry_schema_dsl, name, yaml_schema, locales)
          super

          dry_schema_dsl.required(:values).hash do |schema_dsl|
            next unless yaml_schema['values'].is_a? Hash

            yaml_schema['values'].each_key do |value|
              schema_dsl.required(value.to_sym).hash do |value_schema_dsl|
                locales.each do |locale|
                  Locale.to_schema(value_schema_dsl, locale, nil, nil)
                end
              end
            end
          end
        end

        # A Predefined Array is optional or required, but when required
        # needs to contain Strings defined on the YAML schema
        #
        # @param :dry_schema_dsl [Dry::Schema::DSL]
        # @param :name [Symbol]
        # @param :yaml_schema [Hash]
        def to_document_schema(dry_schema_dsl, name, yaml_schema)
          dry_schema_dsl.public_send(optional_or_required(yaml_schema),
                                     name).array(included_in?: yaml_schema['values'].keys)
          dry_schema_dsl.before(:value_coercer) do |result|
            next unless result[name].is_a? ::Array

            result.to_h.tap do |r|
              r[name] = Sutty::Schema::Types::PlainTextArray[r[name]]
            end
          end
        end
      end
    end
  end
end
