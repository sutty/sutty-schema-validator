# frozen_string_literal: true

require 'jekyll/utils'
require_relative '../attributes'

module Sutty
  module Schema
    module Attributes
      # Validate String schemas
      module Slug
        extend Sutty::Schema::Attributes

        module_function

        # The schema can also contain a slugify mode
        #
        # @param :dry_schema_dsl [Dry::Schema::DSL]
        # @param :name [Symbol]
        # @param :yaml_schema [Hash]
        # @param :locales [Array]
        def to_schema(dry_schema_dsl, _name, yaml_schema, locales)
          super

          dry_schema_dsl.optional(:mode).filled(:string, included_in?: Jekyll::Utils::SLUGIFY_MODES)
        end

        # A slug is always required
        #
        # @param :dry_schema_dsl [Dry::Schema::DSL]
        # @param :name [Symbol]
        # @param :yaml_schema [Hash]
        # @param :locale [Symbol]
        def to_document_schema(dry_schema_dsl, name, yaml_schema, _locale)
          mode = yaml_schema['mode'] || 'default'

          dry_schema_dsl.required(name).filled(:"slug_#{mode}")
        end
      end
    end
  end
end
