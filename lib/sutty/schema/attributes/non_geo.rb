# frozen_string_literal: true

require_relative '../attributes'

module Sutty
  module Schema
    module Attributes
      # Validate non_Geo schemas
      module NonGeo
        extend Sutty::Schema::Attributes

        module_function

        # Complete the schema with label and help, and locale for each
        # possible value.
        #
        # @param :dry_schema_dsl [Dry::Schema::DSL]
        # @param :name [Symbol]
        # @param :yaml_schema [Hash]
        def to_schema(dry_schema_dsl, name, yaml_schema, locales)
          super

          dry_schema_dsl.required(:lat).hash do |schema_dsl|
            Label.to_schema(schema_dsl, nil, nil, locales)
            Help.to_schema(schema_dsl, nil, nil, locales)
          end

          dry_schema_dsl.required(:lng).hash do |schema_dsl|
            Label.to_schema(schema_dsl, nil, nil, locales)
            Help.to_schema(schema_dsl, nil, nil, locales)
          end
        end


        # A Non_Geo is optional or required, but when required needs to
        # be not empty.
        #
        # @param :dry_schema_dsl [Dry::Schema::DSL]
        # @param :name [Symbol]
        # @param :yaml_schema [Hash]
        def to_document_schema(dry_schema_dsl, name, yaml_schema)
          dry_schema_dsl.public_send(optional_or_required(yaml_schema), name).hash do |dry_schema|
            dry_schema.required(:lat).filled(:float)
            dry_schema.required(:lng).filled(:float)
          end
        end
      end
    end
  end
end