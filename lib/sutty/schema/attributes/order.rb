# frozen_string_literal: true

require_relative '../attributes'

module Sutty
  module Schema
    module Attributes
      # Validate Order schemas
      module Order
        extend Sutty::Schema::Attributes

        module_function

        # Order is always required
        #
        # @param :dry_schema_dsl [Dry::Schema::DSL]
        # @param :name [Symbol]
        # @param :yaml_schema [Hash]
        def to_document_schema(dry_schema_dsl, name, _yaml_schema)
          dry_schema_dsl.required(name).filled(:integer)
        end
      end
    end
  end
end
