# frozen_string_literal: true

module Sutty
  module Schema
    module Attributes
      # Help are human-readable longer description for
      # attributes. Validation requires a list of locales supported.
      module Help
        extend Sutty::Schema::Attributes

        module_function

        def to_schema(dry_schema_dsl, _name, _, locales)
          dry_schema_dsl.required(:help).hash do |dry_schema|
            locales.each do |locale|
              dry_schema.required(locale).filled(:string)
            end
          end
        end
      end
    end
  end
end
