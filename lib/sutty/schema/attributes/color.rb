# frozen_string_literal: true

require_relative '../attributes'
require_relative 'label'
require_relative 'help'

module Sutty
  module Schema
    module Attributes
      # Validate Color schemas
      module Color
        extend Sutty::Schema::Attributes

        module_function

        # A Color is optional or required, but when required needs to
        # be not empty and a valid hexadecimal value.
        #
        # @param :dry_schema_dsl [Dry::Schema::DSL]
        # @param :name [Symbol]
        # @param :yaml_schema [Hash]
        def to_document_schema(dry_schema_dsl, name, yaml_schema)
          dry_schema_dsl.public_send(optional_or_required(yaml_schema), name).filled(:color)
        end
      end
    end
  end
end
