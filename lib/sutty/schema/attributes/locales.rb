# frozen_string_literal: true

require_relative '../attributes'

module Sutty
  module Schema
    module Attributes
      # Validate Locales schemas
      module Locales
        extend Sutty::Schema::Attributes

        module_function

        # A Locales is optional or required, but when required needs to
        # contain Strings.
        #
        # @param :dry_schema_dsl [Dry::Schema::DSL]
        # @param :name [Symbol]
        # @param :yaml_schema [Hash]
        def to_document_schema(dry_schema_dsl, name, yaml_schema)
          dry_schema_dsl.public_send(optional_or_required(yaml_schema), name).value(:unique_array)
        end
      end
    end
  end
end
