# frozen_string_literal: true

require_relative '../attributes'
require_relative 'filter'

module Sutty
  module Schema
    module Attributes
      # Validate RelatedPosts schemas
      module RelatedPosts
        extend Sutty::Schema::Attributes

        module_function

        def to_schema(dry_schema_dsl, name, yaml_schema, locales)
          super

          Filter.to_schema(dry_schema_dsl, name, yaml_schema, locales)
        end

        # Related posts are optional or required, but when required needs to
        # contain UUIDs
        #
        # @param :dry_schema_dsl [Dry::Schema::DSL]
        # @param :name [Symbol]
        # @param :yaml_schema [Hash]
        def to_document_schema(dry_schema_dsl, name, yaml_schema)
          dry_schema_dsl.public_send(optional_or_required(yaml_schema), name).value(:unique_array)
        end
      end
    end
  end
end
