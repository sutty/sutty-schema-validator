# frozen_string_literal: true

require_relative '../attributes'

module Sutty
  module Schema
    module Attributes
      # Validate schemas
      module PredefinedValue
        extend Sutty::Schema::Attributes

        module_function

        # Complete the schema with label and help.  The values hash is
        # composed of locales and their values.  The first locale is
        # used to validate the others, so if a key is missing or added
        # later, the validator can notify it.
        #
        # @todo Support other types
        # @param :dry_schema_dsl [Dry::Schema::DSL]
        # @param :name [Symbol]
        # @param :yaml_schema [Hash]
        def to_schema(dry_schema_dsl, name, yaml_schema, locales)
          super

          dry_schema_dsl.required(:values).hash do |schema_dsl|
            next unless yaml_schema['values'].is_a? Hash

            default_locale = locales.first.to_s

            yaml_schema['values'].each_key do |_value|
              locales.each do |locale|
                next unless yaml_schema['values'][locale.to_s].is_a? Hash

                schema_dsl.required(locale).hash do |locale_schema_dsl|
                  yaml_schema['values'][default_locale].each_key do |key|
                    locale_schema_dsl.required(key.to_sym).filled(:string)
                  end
                end
              end
            end
          end
        end

        # A String is optional or required, but when required needs to
        # be not empty.
        #
        # @param :dry_schema_dsl [Dry::Schema::DSL]
        # @param :name [Symbol]
        # @param :yaml_schema [Hash]
        def to_document_schema(dry_schema_dsl, name, yaml_schema)
          dry_schema_dsl
            .public_send(optional_or_required(yaml_schema), name)
            .value(:plain_text, included_in?: yaml_schema['values'].first.last.keys)
        end
      end
    end
  end
end
