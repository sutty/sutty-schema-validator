# frozen_string_literal: true

require_relative '../attributes'

module Sutty
  module Schema
    module Attributes
      # Validate File schemas
      module File
        extend Sutty::Schema::Attributes

        module_function

        # Complete the schema with path and description's label and help.
        #
        # @param :dry_schema_dsl [Dry::Schema::DSL]
        # @param :name [Symbol]
        # @param :yaml_schema [Hash]
        def to_schema(dry_schema_dsl, name, yaml_schema, locales)
          dry_schema_dsl.required(:path).hash do |dry_schema|
            Label.to_schema(dry_schema, name, yaml_schema, locales)
            Help.to_schema(dry_schema, name, yaml_schema, locales)
          end

          dry_schema_dsl.required(:description).hash do |dry_schema|
            Label.to_schema(dry_schema, name, yaml_schema, locales)
            Help.to_schema(dry_schema, name, yaml_schema, locales)
          end
        end

        # A File is optional or required, but when required the path
        # can't be empty. Description is entirely optional.
        #
        # @todo How to specify current directory?
        # @param :dry_schema_dsl [Dry::Schema::DSL]
        # @param :name [Symbol]
        # @param :yaml_schema [Hash]
        def to_document_schema(dry_schema_dsl, name, yaml_schema)
          dry_schema_dsl.public_send(optional_or_required(yaml_schema), name).hash do |dry_schema|
            dry_schema.required(:path) do
              filled? & str? & exist? & not_git_lfs_pointer?
            end

            dry_schema.optional(:description).maybe(:string)
          end
        end
      end
    end
  end
end
