# frozen_string_literal: true

require_relative '../attributes'

module Sutty
  module Schema
    module Attributes
      # Validate Markdown_content schemas
      module MarkdownContent
        extend Sutty::Schema::Attributes

        module_function

        # Complete the schema with label, help and private.
        #
        # @param :dry_schema_dsl [Dry::Schema::DSL]
        # @param :name [Symbol]
        # @param :yaml_schema [Hash]
        def to_schema(dry_schema_dsl, name, yaml_schema, locales)
          super

          Private.to_schema(dry_schema_dsl, nil, nil)
        end


        # A MarkdownContent is optional or required, but when required needs to
        # be not empty.
        #
        # @param :dry_schema_dsl [Dry::Schema::DSL]
        # @param :name [Symbol]
        # @param :yaml_schema [Hash]
        def to_document_schema(dry_schema_dsl, name, yaml_schema)
          dry_schema_dsl.public_send(optional_or_required(yaml_schema), name).filled(:plain_text)
        end
      end
    end
  end
end
