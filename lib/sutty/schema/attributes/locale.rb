# frozen_string_literal: true

module Sutty
  module Schema
    module Attributes
      # Labels are attribute translations
      module Locale
        extend Sutty::Schema::Attributes

        module_function

        def to_schema(dry_schema_dsl, locale, _, _)
          dry_schema_dsl.required(locale).filled(:string)
        end
      end
    end
  end
end
