# frozen_string_literal: true

module Sutty
  module Schema
    module Attributes
      # Filters are association conditions
      module Filter
        extend Sutty::Schema::Attributes

        module_function

        # Filters are hashes whose values can be strings or
        # array of strings.
        def to_schema(dry_schema_dsl, _, yaml_schema, _)
          dry_schema_dsl.optional(:filter).hash do |dry_schema|
            if yaml_schema['filter'].is_a? ::Hash
              yaml_schema['filter'].each_key do |key|
                dry_schema.required(key.to_sym) do
                  str? ^ (array? & each(:string))
                end
              end
            else
              # XXX: Create a rule so the hash validation fails
              dry_schema.required(:something)
            end
          end
        end
      end
    end
  end
end
