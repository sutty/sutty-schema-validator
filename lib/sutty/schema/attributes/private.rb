# frozen_string_literal: true

module Sutty
  module Schema
    module Attributes
      # Private is an attribute to cypher text content.
      module Private
        extend Sutty::Schema::Attributes

        module_function

        def to_schema(dry_schema_dsl, _, _)
          dry_schema_dsl.optional(:private).value(:bool)
        end
      end
    end
  end
end
