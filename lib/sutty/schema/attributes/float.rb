# frozen_string_literal: true

require_relative '../attributes'

module Sutty
  module Schema
    module Attributes
      # Validate Float schemas
      module Float
        extend Sutty::Schema::Attributes

        module_function

        # A Float is optional or required, but when required needs to
        # be not empty.
        #
        # @param :dry_schema_dsl [Dry::Schema::DSL]
        # @param :name [Symbol]
        # @param :yaml_schema [Hash]
        def to_document_schema(dry_schema_dsl, name, yaml_schema)
          dry_schema_dsl.public_send(optional_or_required(yaml_schema), name).filled(:float)
        end
      end
    end
  end
end
