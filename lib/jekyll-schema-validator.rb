# frozen_string_literal: true

require 'sutty/schema/reserved_keys_validator'
require 'sutty/schema/schema_validator'
require 'sutty/schema/document_validator'

# Initializes Sutty schemas and reports status as soon as data is
# available.
Jekyll::Hooks.register :site, :post_read, priority: :high do |site|
  PREFIX = 'Schema:'
  PREFIX_WITH_MORE_MESSAGES =
    begin
      raise NameError unless defined? Jekyll::LogAdapters::Tap

      '┍ Schema:'
    rescue NameError
      'Schema ┑'
    end

  schema_validators = {}
  valid_layouts = site.config['locales'].to_h do |locale|
    [
      locale,
      site.data['layouts'].keys.to_h do |layout|
        [layout, layout]
      end
    ]
  end

  site.data['layouts'].each do |layout, yaml_schema|
    reserved_keys_validator = Sutty::Schema::ReservedKeysValidator.new(layout, yaml_schema)

    if reserved_keys_validator.valid?
      Jekyll.logger.info PREFIX, "#{layout} keys are valid"
    else
      reserved_keys = Sutty::Schema::ReservedKeysValidator::RESERVED_KEYS & yaml_schema.keys

      Jekyll.logger.error PREFIX_WITH_MORE_MESSAGES, "#{layout} contains reserved keys, please rename them"

      reserved_keys.each do |reserved_key|
        Jekyll.logger.error (reserved_key == reserved_keys.last ? '└' : '├'), reserved_key
      end
    end

    schema_validators[layout] =
      schema_validator = Sutty::Schema::SchemaValidator.new(layout, yaml_schema, site.config['locales'])

    if schema_validator.valid?
      Jekyll.logger.info PREFIX, "#{layout} is valid"
    else
      Jekyll.logger.warn PREFIX_WITH_MORE_MESSAGES, "#{layout} is not valid"

      last = schema_validator.validate.errors.messages.last

      schema_validator.validate.errors.messages.each do |message|
        Jekyll.logger.warn (message == last ? '└' : '├'),
                           "#{message.path.map(&:to_s).join('.')} #{message.text}"
      end
    end

    # Since these attributes are added automatically by Jekyll, we set
    # them when missing so documents can be validated later on.
    yaml_schema['tags'] ||= { 'type' => 'array' }
    yaml_schema['categories'] ||= { 'type' => 'array' }
    yaml_schema['draft'] ||= { 'type' => 'boolean' }
    yaml_schema['layout'] ||= { 'type' => 'predefined_value', 'required' => true, 'values' => valid_layouts }
    yaml_schema['slug'] ||= { 'type' => 'string' }
    yaml_schema['ext'] ||= { 'type' => 'string' }
    yaml_schema['date'] ||= { 'type' => 'time' }
    yaml_schema['excerpt'] ||= { 'type' => 'excerpt' }
  end

  site.config['schema_validators'] = schema_validators
end

# Validates each document according to the schema set on it's layout
Jekyll::Hooks.register :documents, :pre_render, priority: :high do |document|
  site = document.site
  layout = document['layout']

  # Document needs a layout
  unless layout.is_a?(String) && !layout.strip.empty? && !document.no_layout?
    Jekyll.logger.warn PREFIX, "#{document.relative_path} doesn't have a layout"
    next
  end

  schema_validator = site.config.dig('schema_validators', layout)

  unless schema_validator
    Jekyll.logger.warn PREFIX,
                       "#{document.relative_path} doesn't have a schema to validate against, create _data/layouts/#{layout}.yml"
    next
  end

  document_validator = Sutty::Schema::DocumentValidator.new(document, site.data.dig('layouts', layout))

  if document_validator.valid?
    Jekyll.logger.info PREFIX, "#{document.relative_path} valid"
  else
    Jekyll.logger.warn PREFIX_WITH_MORE_MESSAGES, "#{document.relative_path} is not valid"

    last = document_validator.validate.errors.messages.last
    document_validator.validate.errors.messages.each do |message|
      Jekyll.logger.warn (message == last ? '└' : '├'),
                         "#{message.path.map(&:to_s).join('.')} #{message.text}"
    end
  end
end
