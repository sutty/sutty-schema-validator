# Sutty Schema Validator

Validates Sutty's schemas ("layouts") and Jekyll posts based on them.

## Validate during Jekyll builds

Add the `jekyll-schema-validator` plugin and an array of locales to
`_config.yml`:

```yaml
plugins:
- "jekyll-schema-validator"
locales:
- "en"
```

Sutty schemas are used to validate and report Jekyll documents ("posts")
during build.

## Development

After checking out the repo, run `bundle` to install dependencies.

To release a new version, update the version number in
`sutty-schema-validator.gemspec`, and then run `go-task release`, which
will push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
<https://0xacab.org/sutty/sutty-schema-validator>. This
project is intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty code of
conduct](https://sutty.nl/en/code-of-conduct/).

## License

The gem is available as free software under the terms of the
MIT-Antifascist License.

## Code of Conduct

Everyone interacting in the sutty-schema-validator project’s
codebases, issue trackers, chat rooms and mailing lists is expected to
follow the [code of conduct](https://sutty.nl/en/code-of-conduct/).
