# frozen_string_literal: true

require 'spec_helper'
require 'sutty/schema/reserved_keys_validator'

RSpec.describe Sutty::Schema::ReservedKeysValidator do
  describe '#new' do
    it 'can be initialized' do
      expect(
        Sutty::Schema::ReservedKeysValidator.new(SecureRandom.hex, {}).class
      ).to(eql(Sutty::Schema::ReservedKeysValidator))
    end

    it 'has a name' do
      random_name = SecureRandom.hex

      expect(
        Sutty::Schema::ReservedKeysValidator.new(random_name, {}).name
      ).to(eql(random_name.to_sym))
    end

    it 'has yaml schemas' do
      random_yaml =
        (1..100).to_a.sample.times.map { SecureRandom.hex(1) }.zip(
          (1..100).to_a.sample.times.map { SecureRandom.hex(1) }
        ).to_h

      expect(
        Sutty::Schema::ReservedKeysValidator.new(SecureRandom.hex, random_yaml).yaml_schema
      ).to(eql(random_yaml))
    end
  end

  describe '#dry_schema' do
    it 'can generate a DRY schema from YAML' do
      expect(
        Sutty::Schema::ReservedKeysValidator.new(SecureRandom.hex, {}).dry_schema.class
      ).to(eql(Dry::Schema::Params))
    end
  end

  describe '#valid?' do
    it 'can be valid' do
      yaml_schema = rand(1..100).times.to_h do
        [SecureRandom.hex, {}]
      end

      expect(
        (schema = Sutty::Schema::ReservedKeysValidator.new(SecureRandom.hex, yaml_schema)).valid?
      ).to(eql(true), schema.errors)
    end

    it 'can be invalid' do
      yaml_schema = Sutty::Schema::ReservedKeysValidator::RESERVED_KEYS.to_h do |key|
        [key, {}]
      end

      schema = Sutty::Schema::ReservedKeysValidator.new(SecureRandom.hex, yaml_schema)

      expect(schema.valid?).to(eql(false))

      yaml_schema.keys.map(&:to_sym).each_with_index do |_, i|
        expect(schema.errors[:keys][i]).not_to(eql(nil))
      end
    end
  end

  describe '#validate' do
  end
end
