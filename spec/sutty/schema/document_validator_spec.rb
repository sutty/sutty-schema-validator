# frozen_string_literal: true

require 'spec_helper'
require 'sutty/schema/document_validator'

RSpec.describe Sutty::Schema::DocumentValidator do
  def build_document(data = {})
    StubDocument.new(data: {}.merge(data))
  end

  describe '#new' do
    it 'can be initialized' do
      expect(
        Sutty::Schema::DocumentValidator.new(build_document, {}).class
      ).to(eql(Sutty::Schema::DocumentValidator))
    end

    it 'has a document' do
      document = build_document

      expect(
        Sutty::Schema::DocumentValidator.new(document, {}).document
      ).to(eql(document))
    end

    it 'has yaml schemas' do
      random_yaml =
        (1..100).to_a.sample.times.map { SecureRandom.hex(1) }.zip(
          (1..100).to_a.sample.times.map { SecureRandom.hex(1) }
        ).to_h

      expect(
        Sutty::Schema::DocumentValidator.new(build_document, random_yaml).yaml_schema
      ).to(eql(random_yaml))
    end
  end

  describe '#dry_schema' do
    it 'can generate a DRY schema from YAML' do
      expect(
        Sutty::Schema::DocumentValidator.new(build_document, {}).dry_schema.class
      ).to(eql(Dry::Schema::Params))
    end
  end

  describe '#valid?' do
    it 'can be valid' do
      attribute = SecureRandom.hex

      expect(
        Sutty::Schema::DocumentValidator.new(build_document(attribute => SecureRandom.hex),
                                             { attribute => { 'type' => 'string' } }).valid?
      ).to(eql(true))
    end

    it 'can be invalid' do
      expect(
        Sutty::Schema::DocumentValidator.new(build_document,
                                             { SecureRandom.hex => { 'type' => 'string', 'required' => true } }).valid?
      ).to(eql(false))
    end
  end

  describe '#validate' do
  end
end
