# frozen_string_literal: true

require 'spec_helper'
require 'yaml'
require 'sutty/schema/schema_validator'

RSpec.describe Sutty::Schema::SchemaValidator do
  describe '#new' do
    it 'can be initialized' do
      expect(
        Sutty::Schema::SchemaValidator.new(SecureRandom.hex, {}, []).class
      ).to(eql(Sutty::Schema::SchemaValidator))
    end

    it 'has a name' do
      random_name = SecureRandom.hex

      expect(
        Sutty::Schema::SchemaValidator.new(random_name, {}, []).name
      ).to(eql(random_name.to_sym))
    end

    it 'has yaml schemas' do
      random_yaml =
        (1..100).to_a.sample.times.map { SecureRandom.hex(1) }.zip(
          (1..100).to_a.sample.times.map { SecureRandom.hex(1) }
        ).to_h

      expect(
        Sutty::Schema::SchemaValidator.new(SecureRandom.hex, random_yaml, []).yaml_schema
      ).to(eql(random_yaml))
    end

    it 'has locales' do
      random_locales = (1..100).to_a.sample.times.map { SecureRandom.hex(1) }

      expect(
        Sutty::Schema::SchemaValidator.new(SecureRandom.hex, {}, random_locales).locales
      ).to(eql(random_locales.map(&:to_sym)))
    end
  end

  describe '#dry_schema' do
    it 'can generate a DRY schema from YAML' do
      expect(
        Sutty::Schema::SchemaValidator.new(SecureRandom.hex, {}, %w[es en]).dry_schema.class
      ).to(eql(Dry::Schema::Params))
    end
  end

  describe '#valid?' do
    it 'can be valid' do
      yaml_schema = YAML.safe_load(File.read('spec/fixtures/valid_schema.yml'))

      expect(
        (schema = Sutty::Schema::SchemaValidator.new(SecureRandom.hex, yaml_schema, %w[tt])).valid?
      ).to(eql(true), schema.errors)
    end

    it 'can be invalid' do
      yaml_schema = YAML.safe_load(File.read('spec/fixtures/invalid_schema.yml'))
      schema = Sutty::Schema::SchemaValidator.new(SecureRandom.hex, yaml_schema, %w[tt])

      expect(schema.valid?).to(eql(false))

      yaml_schema.keys.map(&:to_sym).each do |attribute|
        expect(schema.errors[attribute]).not_to(eql(nil))
      end
    end
  end

  describe '#validate' do
  end
end
