# frozen_string_literal: true

require_relative 'base'
require 'sutty/schema/attributes/predefined_value'

RSpec.describe Sutty::Schema::Attributes::PredefinedValue do
  include Sutty::Schema::Attributes::Base

  def random_values(locales)
    second = SecureRandom.hex

    {
      'values' => locales.to_h do |locale|
        [
          locale, {
            '' => SecureRandom.hex,
            second => SecureRandom.hex
          }
        ]
      end
    }
  end

  describe '#to_schema' do
    it 'can be valid' do
      dry_schema_dsl = random_schema

      locales = random_locales
      schema = { 'type' => 'predefined_value' }
               .merge(random_label_schema(locales))
               .merge(random_help_schema(locales))
               .merge(random_values(locales))

      Sutty::Schema::Attributes::PredefinedValue.to_schema(
        dry_schema_dsl,
        SecureRandom.hex.to_sym,
        schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(schema).success?).to(eql(true))
    end

    it 'can be required' do
      dry_schema_dsl = random_schema

      locales = random_locales
      schema = { 'type' => 'predefined_value', 'required' => true }
               .merge(random_label_schema(locales))
               .merge(random_help_schema(locales))
               .merge(random_values(locales))
      name = SecureRandom.hex.to_sym

      Sutty::Schema::Attributes::PredefinedValue.to_schema(
        dry_schema_dsl,
        name,
        schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(schema).success?).to(eql(true))
    end
  end

  describe '#to_document_schema' do
    it 'can validate' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'predefined_value' }.merge(random_values([:tt]))

      Sutty::Schema::Attributes::PredefinedValue.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({ name => schema['values'][:tt].keys.sample }).success?).to(eql(true))
    end

    it 'can be optional' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'predefined_value' }.merge(random_values([:tt]))

      Sutty::Schema::Attributes::PredefinedValue.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(true))
    end

    it 'can be valid' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'predefined_value', 'required' => true }.merge(random_values([:tt]))

      Sutty::Schema::Attributes::PredefinedValue.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(false))
    end

    it 'removes html' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'predefined_value' }.merge(random_values([:tt]))
      clean_content = schema['values'][:tt].keys.sample
      content = "<html attributes=\"something\">#{clean_content}</html>"

      Sutty::Schema::Attributes::PredefinedValue.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({ name => content }).success?).to(eql(true))
      expect(dry_schema_dsl.call.call({ name => content })[name]).to(eql(clean_content))
    end
  end
end
