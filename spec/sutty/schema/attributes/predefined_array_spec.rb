# frozen_string_literal: true

# frozen_literal: true

require_relative 'base'
require 'sutty/schema/attributes/predefined_array'

RSpec.describe Sutty::Schema::Attributes::PredefinedArray do
  include Sutty::Schema::Attributes::Base

  def random_values_schema(locales = random_locales)
    {
      'values' =>
        rand(1..100).times.map do
          [
            SecureRandom.hex,
            locales.map do |locale|
              [locale, SecureRandom.hex]
            end.to_h
          ]
        end.to_h
    }
  end

  describe '#to_schema' do
    it 'can be valid' do
      dry_schema_dsl = random_schema

      locales = random_locales
      schema = { 'type' => 'predefined_array' }
               .merge(random_label_schema(locales))
               .merge(random_help_schema(locales))
               .merge(random_values_schema(locales))

      Sutty::Schema::Attributes::PredefinedArray.to_schema(
        dry_schema_dsl,
        SecureRandom.hex.to_sym,
        schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(schema).success?).to(eql(true))
    end

    it 'can be required' do
      dry_schema_dsl = random_schema

      locales = random_locales
      schema = { 'type' => 'predefined_array', 'required' => true }
               .merge(random_label_schema(locales))
               .merge(random_help_schema(locales))
               .merge(random_values_schema(locales))
      name = SecureRandom.hex.to_sym

      Sutty::Schema::Attributes::PredefinedArray.to_schema(
        dry_schema_dsl,
        name,
        schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(schema).success?).to(eql(true))
    end
  end

  describe '#to_document_schema' do
    it 'can validate' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'predefined_array' }.merge(random_values_schema)

      Sutty::Schema::Attributes::PredefinedArray.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      values = schema['values'].keys
      content = values.sample(rand(2..(values.size)))

      expect(dry_schema_dsl.call.call({ name => content }).success?).to(eql(true))
    end

    it 'can be optional' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'predefined_array' }.merge(random_values_schema)

      Sutty::Schema::Attributes::PredefinedArray.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(true))
    end

    it 'can be valid' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'predefined_array', 'required' => true }.merge(random_values_schema)

      Sutty::Schema::Attributes::PredefinedArray.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(false))
    end

    it 'removes html' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'predefined_array' }.merge(random_values_schema)
      clean_content = schema['values'].keys.sample
      content = "<html attributes=\"something\">#{clean_content}</html>"

      Sutty::Schema::Attributes::PredefinedArray.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({ name => [content] }).success?).to(eql(true))
      expect(dry_schema_dsl.call.call({ name => [content] })[name]).to(eql([clean_content]))
    end

    it 'removes empty values' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      content = []
      clean_content = 0
      schema = { 'type' => 'predefined_array' }.merge(random_values_schema)

      rand(1..100).times do
        if [true, false].sample
          content << schema['values'].keys.sample
          clean_content += 1
        else
          content << [nil, '', "  \n", "\r", "\r\n   ", "\t", '<html></html>'].sample
        end
      end

      Sutty::Schema::Attributes::PredefinedArray.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({ name => content }).success?).to(eql(true))
      expect(dry_schema_dsl.call.call({ name => content })[name].size).to(eql(clean_content))
    end
  end
end
