# frozen_string_literal: true

require_relative 'base'
require 'sutty/schema/attributes/color'

RSpec.describe Sutty::Schema::Attributes::Color do
  include Sutty::Schema::Attributes::Base

  describe '#to_schema' do
    it 'can be valid' do
      dry_schema_dsl = random_schema
      locales = random_locales
      color_schema = { 'type' => 'color' }
                     .merge(random_label_schema(locales))
                     .merge(random_help_schema(locales))

      Sutty::Schema::Attributes::Color.to_schema(
        dry_schema_dsl,
        SecureRandom.hex.to_sym,
        color_schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(color_schema).success?).to(eql(true))
    end

    it 'can be required' do
      dry_schema_dsl = random_schema
      locales = random_locales
      color_schema = { 'type' => 'color', 'required' => true }
                     .merge(random_label_schema(locales))
                     .merge(random_help_schema(locales))
      name = SecureRandom.hex.to_sym

      Sutty::Schema::Attributes::Color.to_schema(
        dry_schema_dsl,
        name,
        color_schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(color_schema).success?).to(eql(true))
    end
  end

  describe '#to_document_schema' do
    it 'can validate' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      color_schema = { 'type' => 'color' }

      Sutty::Schema::Attributes::Color.to_document_schema(
        dry_schema_dsl,
        name,
        color_schema
      )

      expect(dry_schema_dsl.call.call({ name => "##{SecureRandom.hex(3)}" }).success?).to(eql(true))
    end

    it 'can be optional' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      color_schema = { 'type' => 'color' }

      Sutty::Schema::Attributes::Color.to_document_schema(
        dry_schema_dsl,
        name,
        color_schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(true))
    end

    it 'can be invalid' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      color_schema = { 'type' => 'color', 'required' => true }

      Sutty::Schema::Attributes::Color.to_document_schema(
        dry_schema_dsl,
        name,
        color_schema
      )

      expect(dry_schema_dsl.call.call({ name => SecureRandom.hex }).success?).to(eql(false))
    end
  end
end
