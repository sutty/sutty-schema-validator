# frozen_string_literal: true
require_relative 'base'
require 'sutty/schema/attributes/non_geo'

RSpec.describe Sutty::Schema::Attributes::NonGeo do
  include Sutty::Schema::Attributes::Base

  describe '#to_schema' do
    it 'can be valid' do
      dry_schema_dsl = random_schema

      locales = random_locales
      schema = { 'type' => 'non_geo',
                      'lat' => {}.merge(random_label_schema(locales)).merge(random_help_schema(locales)),
                      'lng' => {}.merge(random_label_schema(locales)).merge(random_help_schema(locales))  }
                      .merge(random_label_schema(locales))
                      .merge(random_help_schema(locales))

      Sutty::Schema::Attributes::NonGeo.to_schema(
        dry_schema_dsl,
        SecureRandom.hex.to_sym,
        schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(schema).success?).to(eql(true))
    end

    it 'can be required' do
      dry_schema_dsl = random_schema

      locales = random_locales
      schema = { 'type' => 'non_geo', 'required' => true, 
                      'lat' => {}.merge(random_label_schema(locales)).merge(random_help_schema(locales)),
                      'lng' => {}.merge(random_label_schema(locales)).merge(random_help_schema(locales)) }
                      .merge(random_label_schema(locales))
                      .merge(random_help_schema(locales))

      name = SecureRandom.hex.to_sym

      Sutty::Schema::Attributes::NonGeo.to_schema(
        dry_schema_dsl,
        name,
        schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(schema).success?).to(eql(true))
    end

    it 'must have lat parameter' do 
      dry_schema_dsl = random_schema

      locales = random_locales
      schema = { 'type' => 'non_geo', 'required' => true, 
                      'lng' => {}.merge(random_label_schema(locales)).merge(random_help_schema(locales)) }
                      .merge(random_label_schema(locales))
                      .merge(random_help_schema(locales))

      name = SecureRandom.hex.to_sym

      Sutty::Schema::Attributes::NonGeo.to_schema(
        dry_schema_dsl,
        name,
        schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(schema).success?).to(eql(false))
    end

    it 'must have lng parameter' do 
      dry_schema_dsl = random_schema

      locales = random_locales
      schema = { 'type' => 'non_geo', 'required' => true, 
                      'lat' => {}.merge(random_label_schema(locales)).merge(random_help_schema(locales)) }
                      .merge(random_label_schema(locales))
                      .merge(random_help_schema(locales))

      name = SecureRandom.hex.to_sym

      Sutty::Schema::Attributes::NonGeo.to_schema(
        dry_schema_dsl,
        name,
        schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(schema).success?).to(eql(false))
    end

  end

  describe '#to_document_schema' do
    it 'can validate' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'non_geo' }

      Sutty::Schema::Attributes::NonGeo.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({ name => { 'lat' => 41.23, 'lng' => '-3.09567' } }).success?).to(eql(true))
      expect(dry_schema_dsl.call.call({ name => { 'lat' => '-0.23', 'lng' => 1.09 } }).success?).to(eql(true))
    end

    it 'can be optional' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'non_geo' }

      Sutty::Schema::Attributes::NonGeo.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(true))
    end

    it 'can be valid' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'non_geo', 'required' => true }

      Sutty::Schema::Attributes::NonGeo.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(false))
    end

    it 'have to be a number' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'non_geo' }

      Sutty::Schema::Attributes::NonGeo.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({ name => { 'lat' => '2.15', 'lng' => 'hi' } }).success?).to(eql(false))
      expect(dry_schema_dsl.call.call({ name => { 'lat' => 'true', 'lng' => '14' } }).success?).to(eql(false))
    end

    it 'lat & lng values must be present' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'non_geo' }

      Sutty::Schema::Attributes::NonGeo.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )
      expect(dry_schema_dsl.call.call({ name => { 'lat' => '', 'lng' => -1.0345 } }).success?).to(eql(false))
      expect(dry_schema_dsl.call.call({ name => { 'lat' => 4.1986, 'lng' => '' } }).success?).to(eql(false))
    end

    it 'lat & lng values must be as expected' do 
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'non_geo', 'lat' => '2.01', 'lng' => -1.0345  }

      Sutty::Schema::Attributes::NonGeo.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      result = dry_schema_dsl.call.call({ name => { 'lat' => '2.01', 'lng' => "-1.03" } })
      expect(result[name]).to include(:lat => 2.01, :lng => -1.03)
    end
  end
end
