# frozen_string_literal: true

require_relative 'base'
require 'sutty/schema/attributes/label'

RSpec.describe Sutty::Schema::Attributes::Label do
  include Sutty::Schema::Attributes::Base

  describe '#to_schema' do
    it 'can be valid' do
      dry_schema_dsl = random_schema

      locales = random_locales
      label_schema = random_label_schema(locales)

      Sutty::Schema::Attributes::Label.to_schema(
        dry_schema_dsl,
        SecureRandom.hex,
        label_schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(label_schema).success?).to(eql(true))
    end

    it 'can be invalid' do
      dry_schema_dsl = random_schema
      locales = random_locales
      label_schema = random_label_schema(locales)

      Sutty::Schema::Attributes::Label.to_schema(
        dry_schema_dsl,
        SecureRandom.hex,
        label_schema,
        locales.map(&:to_sym)
      )

      label_schema['label'].keys.sample((1..(label_schema.keys.size)).to_a.sample).each do |k|
        label_schema['label'].delete k
      end

      expect(dry_schema_dsl.call.call(label_schema).success?).to(eql(false))
    end
  end
end
