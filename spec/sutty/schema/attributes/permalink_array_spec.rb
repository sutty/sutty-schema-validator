# frozen_string_literal: true

require_relative 'base'
require 'sutty/schema/attributes/permalink_array'

RSpec.describe Sutty::Schema::Attributes::PermalinkArray do
  include Sutty::Schema::Attributes::Base

  describe '#to_schema' do
    it 'can be valid' do
      dry_schema_dsl = random_schema
      locales = random_locales
      schema = { 'type' => 'permalink_array' }
                         .merge(random_label_schema(locales))
                         .merge(random_help_schema(locales))

      Sutty::Schema::Attributes::PermalinkArray.to_schema(
        dry_schema_dsl,
        SecureRandom.hex.to_sym,
        schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(schema).success?).to(eql(true))
    end

    it 'can be required' do
      dry_schema_dsl = random_schema
      locales = random_locales
      schema = { 'type' => 'permalink_array', 'required' => true }
                         .merge(random_label_schema(locales))
                         .merge(random_help_schema(locales))
      name = SecureRandom.hex.to_sym

      Sutty::Schema::Attributes::PermalinkArray.to_schema(
        dry_schema_dsl,
        name,
        schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(schema).success?).to(eql(true))
    end
  end

  describe '#to_document_schema' do
    it 'can validate' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'permalink_array' }
      content = ['/sutty/27/hola.htm', '/readme.html']

      Sutty::Schema::Attributes::PermalinkArray.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )
      
      expect(dry_schema_dsl.call.call({ name => content }).success?).to(eql(true))
    end

    it 'can be optional' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'permalink_array' }

      Sutty::Schema::Attributes::PermalinkArray.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(true))
    end

    it 'can be invalid' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'permalink_array' }
      content = ['/hola/#quetal/-comoestas', 'sútty.nl-issues-29?']

      Sutty::Schema::Attributes::PermalinkArray.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({ name => content }).success?).to(eql(false))
    end
  end
end
