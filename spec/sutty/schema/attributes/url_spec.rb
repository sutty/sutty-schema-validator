# frozen_string_literal: true

require_relative 'base'
require 'sutty/schema/attributes/url'

RSpec.describe Sutty::Schema::Attributes::Url do
  include Sutty::Schema::Attributes::Base

  describe '#to_schema' do
    it 'can be valid' do
      dry_schema_dsl = random_schema

      locales = random_locales
      schema = { 'type' => 'url' }
               .merge(random_label_schema(locales))
               .merge(random_help_schema(locales))

      Sutty::Schema::Attributes::Url.to_schema(
        dry_schema_dsl,
        SecureRandom.hex.to_sym,
        schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(schema).success?).to(eql(true))
    end

    it 'can be required' do
      dry_schema_dsl = random_schema

      locales = random_locales
      schema = { 'type' => 'url', 'required' => true }
               .merge(random_label_schema(locales))
               .merge(random_help_schema(locales))
      name = SecureRandom.hex.to_sym

      Sutty::Schema::Attributes::Url.to_schema(
        dry_schema_dsl,
        name,
        schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(schema).success?).to(eql(true))
    end
  end

  describe '#to_document_schema' do
    it 'can validate' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'url' }

      Sutty::Schema::Attributes::Url.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({ name => 'https://sutty.nl' }).success?).to(eql(true))
    end

    it 'can be optional' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'url' }

      Sutty::Schema::Attributes::Url.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(true))
    end

    it 'can be a valid url with valid protocol and host' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'url', 'required' => true }

      Sutty::Schema::Attributes::Url.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({ name => 'hola' }).success?).to(eql(false))
      expect(dry_schema_dsl.call.call({ name => 'htt:sutty.nl/hola' }).success?).to(eql(false))
      expect(dry_schema_dsl.call.call({ name => 'https:///' }).success?).to(eql(false))
    end
  end
end
