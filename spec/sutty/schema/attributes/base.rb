# frozen_string_literal: true

require 'spec_helper'

module Sutty
  module Schema
    module Attributes
      module Base
        def random_locales
          rand(1..9).times.map { ('a'..'z').to_a.sample(2).join }
        end

        def random_label_schema(locales = random_locales)
          { 'label' => locales.to_h { |k| [k, SecureRandom.hex] } }
        end

        def random_help_schema(locales = random_locales)
          { 'help' => locales.to_h { |k| [k, SecureRandom.hex] } }
        end

        def random_schema
          Dry::Schema::DSL.new(processor_type: Dry::Schema::Params).tap do |dsl|
            dsl.config.messages.load_paths << ::File.join(::File.dirname(__FILE__), '..', '..', '..', '..', 'config',
                                                          'errors.yml')
          end
        end

        def random_private_schema
          { 'private' => [true, false].sample }
        end
      end
    end
  end
end
