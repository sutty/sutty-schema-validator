# frozen_string_literal: true

require_relative 'base'
require 'sutty/schema/attributes/private'

RSpec.describe Sutty::Schema::Attributes::Private do
  include Sutty::Schema::Attributes::Base

  describe '#to_schema' do
    it 'can be valid' do
      dry_schema_dsl = random_schema

      private_schema = random_private_schema

      Sutty::Schema::Attributes::Private.to_schema(
        dry_schema_dsl,
        SecureRandom.hex,
        private_schema
      )

      expect(dry_schema_dsl.call.call(private_schema).success?).to(eql(true))
    end

  end
end
