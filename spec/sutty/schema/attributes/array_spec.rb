# frozen_string_literal: true

require_relative 'base'
require 'sutty/schema/attributes/array'

RSpec.describe Sutty::Schema::Attributes::Array do
  include Sutty::Schema::Attributes::Base

  describe '#to_schema' do
    it 'can be valid' do
      dry_schema_dsl = random_schema

      locales = random_locales
      string_schema = { 'type' => 'array' }
                      .merge(random_label_schema(locales))
                      .merge(random_help_schema(locales))

      Sutty::Schema::Attributes::Array.to_schema(
        dry_schema_dsl,
        SecureRandom.hex.to_sym,
        string_schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(string_schema).success?).to(eql(true))
    end

    it 'can be required' do
      dry_schema_dsl = random_schema

      locales = random_locales
      string_schema = { 'type' => 'array', 'required' => true }
                      .merge(random_label_schema(locales))
                      .merge(random_help_schema(locales))
      name = SecureRandom.hex.to_sym

      Sutty::Schema::Attributes::Array.to_schema(
        dry_schema_dsl,
        name,
        string_schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(string_schema).success?).to(eql(true))
    end
  end

  describe '#to_document_schema' do
    it 'can validate' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      string_schema = { 'type' => 'array' }

      Sutty::Schema::Attributes::Array.to_document_schema(
        dry_schema_dsl,
        name,
        string_schema
      )

      expect(dry_schema_dsl.call.call({ name => [SecureRandom.hex] }).success?).to(eql(true))
    end

    it 'can be optional' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      string_schema = { 'type' => 'array' }

      Sutty::Schema::Attributes::Array.to_document_schema(
        dry_schema_dsl,
        name,
        string_schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(true))
    end

    it 'can be valid' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      string_schema = { 'type' => 'array', 'required' => true }

      Sutty::Schema::Attributes::Array.to_document_schema(
        dry_schema_dsl,
        name,
        string_schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(false))
    end

    it 'removes html' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      clean_content = SecureRandom.hex
      content = "<html attributes=\"something\">#{clean_content}</html>"
      string_schema = { 'type' => 'array' }

      Sutty::Schema::Attributes::Array.to_document_schema(
        dry_schema_dsl,
        name,
        string_schema
      )

      expect(dry_schema_dsl.call.call({ name => [content] }).success?).to(eql(true))
      expect(dry_schema_dsl.call.call({ name => [content] })[name]).to(eql([clean_content]))
    end

    it 'removes empty values' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      content = []
      clean_content = 0

      rand(1..100).times do
        if [true, false].sample
          content << [rand(1..100), SecureRandom.hex].sample
          clean_content += 1
        else
          content << [nil, '', "  \n", "\r", "\r\n   ", "\t", '<html></html>'].sample
        end
      end

      string_schema = { 'type' => 'array' }

      Sutty::Schema::Attributes::Array.to_document_schema(
        dry_schema_dsl,
        name,
        string_schema
      )

      expect(dry_schema_dsl.call.call({ name => content }).success?).to(eql(true))
      expect(dry_schema_dsl.call.call({ name => content })[name].size).to(eql(clean_content))
    end
  end
end
