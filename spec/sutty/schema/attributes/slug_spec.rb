# frozen_string_literal: true

require_relative 'base'
require 'sutty/schema/attributes/slug'

RSpec.describe Sutty::Schema::Attributes::Slug do
  include Sutty::Schema::Attributes::Base

  describe '#to_schema' do
    it 'can be valid' do
      dry_schema_dsl = random_schema

      locales = random_locales
      schema = { 'type' => 'slug', 'mode' => 'default' }
               .merge(random_label_schema(locales))
               .merge(random_help_schema(locales))

      Sutty::Schema::Attributes::Slug.to_schema(
        dry_schema_dsl,
        SecureRandom.hex.to_sym,
        schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(schema).success?).to(eql(true))
    end

    it 'is required' do
      dry_schema_dsl = random_schema

      locales = random_locales
      schema = { 'type' => 'slug', 'mode' => 'default', 'required' => true }
               .merge(random_label_schema(locales))
               .merge(random_help_schema(locales))
      name = SecureRandom.hex.to_sym

      Sutty::Schema::Attributes::Slug.to_schema(
        dry_schema_dsl,
        name,
        schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(schema).success?).to(eql(true))
    end
  end

  describe '#to_document_schema' do
    it 'can validate' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'slug', 'mode' => 'default' }

      Sutty::Schema::Attributes::Slug.to_document_schema(
        dry_schema_dsl,
        name,
        schema,
        :tt
      )

      expect(dry_schema_dsl.call.call({ name => SecureRandom.hex }).success?).to(eql(true))
    end

    it 'slugifies' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'slug', 'mode' => 'default' }
      content = rand(1..10).times.map do
        SecureRandom.hex
      end

      Sutty::Schema::Attributes::Slug.to_document_schema(
        dry_schema_dsl,
        name,
        schema,
        :tt
      )

      expect(dry_schema_dsl.call.call({ name => content.join(" ") }).success?).to(eql(true))
      expect(dry_schema_dsl.call.call({ name => content.join(" ") })[name]).to(eql(content.join("-")))
    end

    it 'removes html' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      clean_content = SecureRandom.hex
      content = "<html attributes=\"something\">#{clean_content}</html>"
      schema = { 'type' => 'slug', 'mode' => 'default' }

      Sutty::Schema::Attributes::Slug.to_document_schema(
        dry_schema_dsl,
        name,
        schema,
        :tt
      )

      expect(dry_schema_dsl.call.call({ name => content }).success?).to(eql(true))
      expect(dry_schema_dsl.call.call({ name => content })[name]).to(eql(clean_content))
    end
  end
end
