# frozen_string_literal: true

require_relative 'base'
require 'sutty/schema/attributes/help'

RSpec.describe Sutty::Schema::Attributes::Help do
  include Sutty::Schema::Attributes::Base

  describe '#to_schema' do
    it 'can be valid' do
      dry_schema_dsl = random_schema

      locales = random_locales
      help_schema = random_help_schema(locales)

      Sutty::Schema::Attributes::Help.to_schema(
        dry_schema_dsl,
        SecureRandom.hex,
        help_schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(help_schema).success?).to(eql(true))
    end

    it 'can be invalid' do
      dry_schema_dsl = random_schema
      locales = random_locales
      help_schema = random_help_schema(locales)

      Sutty::Schema::Attributes::Help.to_schema(
        dry_schema_dsl,
        SecureRandom.hex,
        help_schema,
        locales.map(&:to_sym)
      )

      help_schema['help'].keys.sample((1..(help_schema.keys.size)).to_a.sample).each do |k|
        help_schema['help'].delete k
      end

      expect(dry_schema_dsl.call.call(help_schema).success?).to(eql(false))
    end
  end
end
