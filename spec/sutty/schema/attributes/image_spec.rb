# frozen_string_literal: true

require_relative 'base'
require 'sutty/schema/attributes/image'

RSpec.describe Sutty::Schema::Attributes::Image do
  include Sutty::Schema::Attributes::Base

  describe '#to_schema' do
    it 'can be valid' do
      dry_schema_dsl = random_schema

      locales = random_locales
      file_schema = {
        'type' => 'image',
        'path' => {}.merge(random_label_schema(locales)).merge(random_help_schema(locales)),
        'description' => {}.merge(random_label_schema(locales)).merge(random_help_schema(locales))
      }

      Sutty::Schema::Attributes::Image.to_schema(
        dry_schema_dsl,
        SecureRandom.hex.to_sym,
        file_schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(file_schema).success?).to(eql(true))
    end

    it 'can be required' do
      dry_schema_dsl = random_schema

      locales = random_locales
      file_schema = { 'type' => 'image', 'required' => true,
                      'path' => {}.merge(random_label_schema(locales)).merge(random_help_schema(locales)),
                      'description' => {}.merge(random_label_schema(locales)).merge(random_help_schema(locales)) }
      name = SecureRandom.hex.to_sym

      Sutty::Schema::Attributes::Image.to_schema(
        dry_schema_dsl,
        name,
        file_schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(file_schema).success?).to(eql(true))
    end
  end

  describe '#to_document_schema' do
    it 'can validate' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      file_schema = { 'type' => 'image' }

      Sutty::Schema::Attributes::Image.to_document_schema(
        dry_schema_dsl,
        name,
        file_schema
      )

      expect(dry_schema_dsl.call.call({ name => { 'path' => 'spec/fixtures/sutty.png' } }).success?).to(eql(true))
    end

    it 'can be optional' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      file_schema = { 'type' => 'image' }

      Sutty::Schema::Attributes::Image.to_document_schema(
        dry_schema_dsl,
        name,
        file_schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(true))
    end

    it 'has an optional description' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      file_schema = { 'type' => 'image' }

      Sutty::Schema::Attributes::Image.to_document_schema(
        dry_schema_dsl,
        name,
        file_schema
      )

      expect(dry_schema_dsl.call.call({ name => { 'path' => 'spec/fixtures/sutty.png',
                                                  'description' => SecureRandom.hex } }).success?).to(eql(true))
      expect(dry_schema_dsl.call.call({ name => { 'path' => 'spec/fixtures/sutty.png',
                                                  'description' => nil } }).success?).to(eql(true))
      expect(dry_schema_dsl.call.call({ name => { 'path' => 'spec/fixtures/sutty.png',
                                                  'description' => '' } }).success?).to(eql(true))
      expect(dry_schema_dsl.call.call({ name => { 'path' => 'spec/fixtures/sutty.png' } }).success?).to(eql(true))
    end

    it 'can be required but description is always optional' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      file_schema = { 'type' => 'image', 'required' => true }

      Sutty::Schema::Attributes::Image.to_document_schema(
        dry_schema_dsl,
        name,
        file_schema
      )

      expect(dry_schema_dsl.call.call({ name => { 'description' => SecureRandom.hex } }).success?).to(eql(false))
      expect(dry_schema_dsl.call.call({ name => { 'path' => 'spec/fixtures/sutty.png',
                                                  'description' => SecureRandom.hex } }).success?).to(eql(true))
      expect(dry_schema_dsl.call.call({ name => { 'path' => 'spec/fixtures/sutty.png',
                                                  'description' => nil } }).success?).to(eql(true))
      expect(dry_schema_dsl.call.call({ name => { 'path' => 'spec/fixtures/sutty.png',
                                                  'description' => '' } }).success?).to(eql(true))
      expect(dry_schema_dsl.call.call({ name => { 'path' => 'spec/fixtures/sutty.png' } }).success?).to(eql(true))
    end

    it 'can be valid' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      file_schema = { 'type' => 'image', 'required' => true }

      Sutty::Schema::Attributes::Image.to_document_schema(
        dry_schema_dsl,
        name,
        file_schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(false))
    end

    it 'needs to be a file and not a pointer' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      file_schema = { 'type' => 'image' }

      Sutty::Schema::Attributes::Image.to_document_schema(
        dry_schema_dsl,
        name,
        file_schema
      )

      expect(dry_schema_dsl.call.call({ name => { 'path' => 'spec/fixtures/git_lfs_pointer.jpg' } }).success?).to(eql(false))
    end

    it 'needs to be a web image' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      file_schema = { 'type' => 'image' }

      Sutty::Schema::Attributes::Image.to_document_schema(
        dry_schema_dsl,
        name,
        file_schema
      )

      expect(dry_schema_dsl.call.call({ name => { 'path' => 'spec/fixtures/valid_schema.yml' } }).success?).to(eql(false))
    end
  end
end
