# frozen_string_literal: true

require_relative 'base'
require 'sutty/schema/attributes/order'

RSpec.describe Sutty::Schema::Attributes::Order do
  include Sutty::Schema::Attributes::Base

  describe '#to_schema' do
    it 'can be valid' do
      dry_schema_dsl = random_schema

      locales = random_locales
      string_schema = { 'type' => 'order' }
                      .merge(random_label_schema(locales))
                      .merge(random_help_schema(locales))

      Sutty::Schema::Attributes::Order.to_schema(
        dry_schema_dsl,
        SecureRandom.hex.to_sym,
        string_schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(string_schema).success?).to(eql(true))
    end

    it 'can be required' do
      dry_schema_dsl = random_schema

      locales = random_locales
      string_schema = { 'type' => 'order', 'required' => true }
                      .merge(random_label_schema(locales))
                      .merge(random_help_schema(locales))
      name = SecureRandom.hex.to_sym

      Sutty::Schema::Attributes::Order.to_schema(
        dry_schema_dsl,
        name,
        string_schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(string_schema).success?).to(eql(true))
    end
  end

  describe '#to_document_schema' do
    it 'can validate' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      string_schema = { 'type' => 'order' }

      Sutty::Schema::Attributes::Order.to_document_schema(
        dry_schema_dsl,
        name,
        string_schema
      )

      expect(dry_schema_dsl.call.call({ name => rand(0..100) }).success?).to(eql(true))
    end

    it 'cannot be optional' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      string_schema = { 'type' => 'order' }

      Sutty::Schema::Attributes::Order.to_document_schema(
        dry_schema_dsl,
        name,
        string_schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(false))
    end

    it 'can be valid' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      string_schema = { 'type' => 'order', 'required' => true }

      Sutty::Schema::Attributes::Order.to_document_schema(
        dry_schema_dsl,
        name,
        string_schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(false))
    end
  end
end
