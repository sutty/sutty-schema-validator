# frozen_string_literal: true

require_relative 'base'
require 'sutty/schema/attributes/email'

RSpec.describe Sutty::Schema::Attributes::Email do
  include Sutty::Schema::Attributes::Base

  describe '#to_schema' do
    it 'can be valid' do
      dry_schema_dsl = random_schema

      locales = random_locales
      schema = { 'type' => 'email' }
               .merge(random_label_schema(locales))
               .merge(random_help_schema(locales))

      Sutty::Schema::Attributes::Email.to_schema(
        dry_schema_dsl,
        SecureRandom.hex.to_sym,
        schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(schema).success?).to(eql(true))
    end

    it 'can be required' do
      dry_schema_dsl = random_schema

      locales = random_locales
      schema = { 'type' => 'email', 'required' => true }
               .merge(random_label_schema(locales))
               .merge(random_help_schema(locales))
      name = SecureRandom.hex.to_sym

      Sutty::Schema::Attributes::Email.to_schema(
        dry_schema_dsl,
        name,
        schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(schema).success?).to(eql(true))
    end
  end

  describe '#to_document_schema' do
    it 'can validate' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'email' }

      Sutty::Schema::Attributes::Email.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({ name => "#{SecureRandom.hex}@gmail.com" }).success?).to(eql(true))
    end

    it 'can be optional' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'email' }

      Sutty::Schema::Attributes::Email.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(true))
    end

    it 'can be required' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'email', 'required' => true }

      Sutty::Schema::Attributes::Email.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(false))
    end

    it 'can be invalid' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'email' }

      Sutty::Schema::Attributes::Email.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({ name => SecureRandom.hex }).success?).to(eql(false))
    end
  end
end
