# frozen_string_literal: true

# frozen_literal: true

require_relative 'base'
require 'sutty/schema/attributes/number'

RSpec.describe Sutty::Schema::Attributes::Number do
  include Sutty::Schema::Attributes::Base

  describe '#to_schema' do
    it 'can be valid' do
      dry_schema_dsl = random_schema

      locales = random_locales
      schema = { 'type' => 'number' }
               .merge(random_label_schema(locales))
               .merge(random_help_schema(locales))

      Sutty::Schema::Attributes::Number.to_schema(
        dry_schema_dsl,
        SecureRandom.hex.to_sym,
        schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(schema).success?).to(eql(true))
    end

    it 'can be required' do
      dry_schema_dsl = random_schema

      locales = random_locales
      schema = { 'type' => 'number', 'required' => true }
               .merge(random_label_schema(locales))
               .merge(random_help_schema(locales))
      name = SecureRandom.hex.to_sym

      Sutty::Schema::Attributes::Number.to_schema(
        dry_schema_dsl,
        name,
        schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(schema).success?).to(eql(true))
    end
  end

  describe '#to_document_schema' do
    it 'can validate' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'number' }

      Sutty::Schema::Attributes::Number.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({ name => rand(1..100) }).success?).to(eql(true))
    end

    it 'can coerce strings to integers' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'number' }

      Sutty::Schema::Attributes::Number.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )
      expect(dry_schema_dsl.call.call({ name => rand(1..100).to_s }).success?).to(eql(true))
    end

    it 'can be optional' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'number' }

      Sutty::Schema::Attributes::Number.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(true))
    end

    it 'can be required' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      schema = { 'type' => 'number', 'required' => true }

      Sutty::Schema::Attributes::Number.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(false))
    end
  end
end
