# frozen_string_literal: true

require_relative 'base'
require 'sutty/schema/attributes/has_and_belongs_to_many'

RSpec.describe Sutty::Schema::Attributes::HasAndBelongsToMany do
  include Sutty::Schema::Attributes::Base

  describe '#to_schema' do
    it 'can be valid' do
      dry_schema_dsl = random_schema

      locales = random_locales
      string_schema = { 'type' => 'has_and_belongs_to_many' }
                      .merge(random_label_schema(locales))
                      .merge(random_help_schema(locales))

      Sutty::Schema::Attributes::HasAndBelongsToMany.to_schema(
        dry_schema_dsl,
        SecureRandom.hex.to_sym,
        string_schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(string_schema).success?).to(eql(true))
    end

    it 'can be required' do
      dry_schema_dsl = random_schema

      locales = random_locales
      string_schema = { 'type' => 'has_and_belongs_to_many', 'required' => true }
                      .merge(random_label_schema(locales))
                      .merge(random_help_schema(locales))
      name = SecureRandom.hex.to_sym

      Sutty::Schema::Attributes::HasAndBelongsToMany.to_schema(
        dry_schema_dsl,
        name,
        string_schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(string_schema).success?).to(eql(true))
    end
  end

  describe '#to_document_schema' do
    it 'can validate' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      string_schema = { 'type' => 'has_and_belongs_to_many' }

      Sutty::Schema::Attributes::HasAndBelongsToMany.to_document_schema(
        dry_schema_dsl,
        name,
        string_schema
      )

      expect(dry_schema_dsl.call.call({ name => [SecureRandom.hex] }).success?).to(eql(true))
    end

    it 'can be optional' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      string_schema = { 'type' => 'has_and_belongs_to_many' }

      Sutty::Schema::Attributes::HasAndBelongsToMany.to_document_schema(
        dry_schema_dsl,
        name,
        string_schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(true))
    end

    it 'can be valid' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      string_schema = { 'type' => 'has_and_belongs_to_many', 'required' => true }

      Sutty::Schema::Attributes::HasAndBelongsToMany.to_document_schema(
        dry_schema_dsl,
        name,
        string_schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(false))
    end

    it 'removes html' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      clean_content = SecureRandom.hex
      content = "<html attributes=\"something\">#{clean_content}</html>"
      string_schema = { 'type' => 'has_and_belongs_to_many' }

      Sutty::Schema::Attributes::HasAndBelongsToMany.to_document_schema(
        dry_schema_dsl,
        name,
        string_schema
      )

      expect(dry_schema_dsl.call.call({ name => [content] }).success?).to(eql(true))
      expect(dry_schema_dsl.call.call({ name => [content] })[name]).to(eql([clean_content]))
    end
  end
end
