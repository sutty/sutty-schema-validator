# frozen_string_literal: true

require_relative 'base'
require 'sutty/schema/attributes/boolean'

RSpec.describe Sutty::Schema::Attributes::Boolean do
  include Sutty::Schema::Attributes::Base

  describe '#to_schema' do
    it 'can be valid' do
      dry_schema_dsl = random_schema
      locales = random_locales
      boolean_schema = { 'type' => 'boolean' }
                       .merge(random_label_schema(locales))
                       .merge(random_help_schema(locales))

      Sutty::Schema::Attributes::Boolean.to_schema(
        dry_schema_dsl,
        SecureRandom.hex.to_sym,
        boolean_schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(boolean_schema).success?).to(eql(true))
    end

    it 'can be required' do
      dry_schema_dsl = random_schema
      locales = random_locales
      boolean_schema = { 'type' => 'boolean', 'required' => true }
                       .merge(random_label_schema(locales))
                       .merge(random_help_schema(locales))
      name = SecureRandom.hex.to_sym

      Sutty::Schema::Attributes::Boolean.to_schema(
        dry_schema_dsl,
        name,
        boolean_schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(boolean_schema).success?).to(eql(true))
    end
  end

  describe '#to_document_schema' do
    it 'can validate' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      boolean_schema = { 'type' => 'boolean' }

      Sutty::Schema::Attributes::Boolean.to_document_schema(
        dry_schema_dsl,
        name,
        boolean_schema
      )

      expect(dry_schema_dsl.call.call({ name => '1' }).success?).to(eql(true))
      expect(dry_schema_dsl.call.call({ name => '0' }).success?).to(eql(true))
      expect(dry_schema_dsl.call.call({ name => 'true' }).success?).to(eql(true))
      expect(dry_schema_dsl.call.call({ name => 'false' }).success?).to(eql(true))
      expect(dry_schema_dsl.call.call({ name => true }).success?).to(eql(true))
      expect(dry_schema_dsl.call.call({ name => false }).success?).to(eql(true))
    end

    it 'can be optional' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      boolean_schema = { 'type' => 'boolean' }

      Sutty::Schema::Attributes::Boolean.to_document_schema(
        dry_schema_dsl,
        name,
        boolean_schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(true))
    end

    it 'can be invalid' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      boolean_schema = { 'type' => 'boolean', 'required' => true }

      Sutty::Schema::Attributes::Boolean.to_document_schema(
        dry_schema_dsl,
        name,
        boolean_schema
      )

      expect(dry_schema_dsl.call.call({ name => SecureRandom.hex }).success?).to(eql(false))
    end
  end
end
