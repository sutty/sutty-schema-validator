# frozen_string_literal: true

require_relative 'base'
require 'sutty/schema/attributes/locales'

RSpec.describe Sutty::Schema::Attributes::Locales do
  include Sutty::Schema::Attributes::Base

  describe '#to_schema' do
    it 'can be valid' do
      dry_schema_dsl = random_schema

      locales = random_locales
      string_schema = { 'type' => 'locales' }
                      .merge(random_label_schema(locales))
                      .merge(random_help_schema(locales))

      Sutty::Schema::Attributes::Locales.to_schema(
        dry_schema_dsl,
        SecureRandom.hex.to_sym,
        string_schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(string_schema).success?).to(eql(true))
    end

    it 'can be required' do
      dry_schema_dsl = random_schema

      locales = random_locales
      string_schema = { 'type' => 'locales', 'required' => true }
                      .merge(random_label_schema(locales))
                      .merge(random_help_schema(locales))
      name = SecureRandom.hex.to_sym

      Sutty::Schema::Attributes::Locales.to_schema(
        dry_schema_dsl,
        name,
        string_schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(string_schema).success?).to(eql(true))
    end
  end

  describe '#to_document_schema' do
    it 'can validate' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      string_schema = { 'type' => 'locales' }

      Sutty::Schema::Attributes::Locales.to_document_schema(
        dry_schema_dsl,
        name,
        string_schema
      )

      expect(dry_schema_dsl.call.call({ name => [SecureRandom.hex] }).success?).to(eql(true))
    end

    it 'can be optional' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      string_schema = { 'type' => 'locales' }

      Sutty::Schema::Attributes::Locales.to_document_schema(
        dry_schema_dsl,
        name,
        string_schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(true))
    end

    it 'can be valid' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      string_schema = { 'type' => 'locales', 'required' => true }

      Sutty::Schema::Attributes::Locales.to_document_schema(
        dry_schema_dsl,
        name,
        string_schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(false))
    end

    it 'removes html' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      clean_content = SecureRandom.hex
      content = "<html attributes=\"something\">#{clean_content}</html>"
      string_schema = { 'type' => 'locales' }

      Sutty::Schema::Attributes::Locales.to_document_schema(
        dry_schema_dsl,
        name,
        string_schema
      )

      expect(dry_schema_dsl.call.call({ name => [content] }).success?).to(eql(true))
      expect(dry_schema_dsl.call.call({ name => [content] })[name]).to(eql([clean_content]))
    end

    it 'removes empty values' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      content = []
      clean_content = 0

      rand(1..100).times do
        if [true, false].sample
          content << SecureRandom.uuid
          clean_content += 1
        else
          content << [nil, '', "  \n", "\r", "\r\n   ", "\t", '<html></html>'].sample
        end
      end

      schema = { 'type' => 'locales' }

      Sutty::Schema::Attributes::RelatedPosts.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({ name => content }).success?).to(eql(true))
      expect(dry_schema_dsl.call.call({ name => content })[name].size).to(eql(clean_content))
    end

    it 'removes duplicate values' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      content = []
      clean_content = 0

      rand(1..100).times do
        if [true, false].sample
          content << SecureRandom.uuid
          clean_content += 1
        else
          content << [nil, '', "  \n", "\r", "\r\n   ", "\t", '<html></html>', content.sample].sample
        end
      end

      schema = { 'type' => 'locales' }

      Sutty::Schema::Attributes::RelatedPosts.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({ name => content }).success?).to(eql(true))
      expect(dry_schema_dsl.call.call({ name => content })[name].size).to(eql(clean_content))
    end

    it 'can contain only strings' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym

      schema = { 'type' => 'locales' }

      Sutty::Schema::Attributes::Locales.to_document_schema(
        dry_schema_dsl,
        name,
        schema
      )

      expect(dry_schema_dsl.call.call({ name => true }).success?).to(eql(false))
      expect(dry_schema_dsl.call.call({ name => -12.78 }).success?).to(eql(false))
      expect(dry_schema_dsl.call.call({ name => 'value1' }).success?).to(eql(false))
    end
  end
end
