# frozen_string_literal: true

require_relative 'base'
require 'sutty/schema/attributes/permalink'

RSpec.describe Sutty::Schema::Attributes::Permalink do
  include Sutty::Schema::Attributes::Base

  describe '#to_schema' do
    it 'can be valid' do
      dry_schema_dsl = random_schema
      locales = random_locales
      permalink_schema = { 'type' => 'permalink' }
                         .merge(random_label_schema(locales))
                         .merge(random_help_schema(locales))

      Sutty::Schema::Attributes::Permalink.to_schema(
        dry_schema_dsl,
        SecureRandom.hex.to_sym,
        permalink_schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(permalink_schema).success?).to(eql(true))
    end

    it 'can be required' do
      dry_schema_dsl = random_schema
      locales = random_locales
      permalink_schema = { 'type' => 'permalink', 'required' => true }
                         .merge(random_label_schema(locales))
                         .merge(random_help_schema(locales))
      name = SecureRandom.hex.to_sym

      Sutty::Schema::Attributes::Permalink.to_schema(
        dry_schema_dsl,
        name,
        permalink_schema,
        locales.map(&:to_sym)
      )

      expect(dry_schema_dsl.call.call(permalink_schema).success?).to(eql(true))
    end
  end

  describe '#to_document_schema' do
    it 'can validate' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      permalink_schema = { 'type' => 'permalink' }

      Sutty::Schema::Attributes::Permalink.to_document_schema(
        dry_schema_dsl,
        name,
        permalink_schema
      )

      expect(dry_schema_dsl.call.call({ name => '/sutty/27/hola.htm' }).success?).to(eql(true))
    end

    it 'can be optional' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      permalink_schema = { 'type' => 'permalink' }

      Sutty::Schema::Attributes::Permalink.to_document_schema(
        dry_schema_dsl,
        name,
        permalink_schema
      )

      expect(dry_schema_dsl.call.call({}).success?).to(eql(true))
    end

    it 'can be invalid' do
      dry_schema_dsl = random_schema

      name = SecureRandom.hex.to_sym
      permalink_schema = { 'type' => 'permalink' }

      Sutty::Schema::Attributes::Permalink.to_document_schema(
        dry_schema_dsl,
        name,
        permalink_schema
      )

      expect(dry_schema_dsl.call.call({ name => 'sútty.nl-issues-29?' }).success?).to(eql(false))
      expect(dry_schema_dsl.call.call({ name => '/hola/#quetal/-comoestas' }).success?).to(eql(false))
    end
  end
end
