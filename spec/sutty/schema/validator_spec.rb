# frozen_string_literal: true

require 'spec_helper'
require 'sutty/schema/validator'

class StubValidator
  include Sutty::Schema::Validator
end

RSpec.describe Sutty::Schema::Validator do
  describe '#dry_schema' do
    it 'needs to be implemented by classes' do
      expect do
        StubValidator.new.dry_schema
      end.to(raise_error(NotImplementedError))
    end
  end

  describe '#validate' do
    it 'needs to be implemented by classes' do
      expect do
        StubValidator.new.validate
      end.to(raise_error(NotImplementedError))
    end
  end

  describe '#camelize' do
    it 'capitalizes single words' do
      expect(StubValidator.new.send(:camelize, 'string')).to(eql('String'))
    end

    it 'converts from snake to camel case' do
      expect(StubValidator.new.send(:camelize, 'a_snakey_string')).to(eql('ASnakeyString'))
    end
  end

  describe '#attribute_for' do
    it 'takes a string and returns an attribute object' do
      expect(StubValidator.new.send(:attribute_for,
                                    'string').ancestors.first).to(eql(Sutty::Schema::Attributes::String))
    end

    it 'returns nil when the string does not map to an attribute object' do
      random_name = ('a'..'z').to_a.sample((1..26).to_a.sample).join

      expect(StubValidator.new.send(:attribute_for, random_name)).to(eql(nil))
    end
  end
end
